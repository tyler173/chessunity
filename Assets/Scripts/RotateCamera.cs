﻿using UnityEngine;
using System.Collections;

public class RotateCamera : MonoBehaviour {

    public float rotationSpeed = 60.0f;
    Screen scr;
    private double xAngle = 0.0f; //angle for axes x for rotation
    private float yAngle = 0.0f;
    Vector2 firstpoint;
    bool canBeSwiped = false;
    // Use this for initialization
    void Start () {
        scr = new Screen();
	}
	
	// Update is called once per frame
	void Update () {

        switch(Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                firstpoint = Input.GetTouch(0).position;
                canBeSwiped = true;
                break;
            case TouchPhase.Ended:
                Vector2 secondpoint = Input.GetTouch(0).position;
                if (firstpoint.x < Screen.width / 4 && Mathf.Abs(secondpoint.y - firstpoint.y) > 200)
                {
                    if (secondpoint.y > firstpoint.y )
                    {
                        StartCoroutine(rotateRight());
                    }
                    else
                    {
                        StartCoroutine(rotateLeft());
                    }
                }
                if (Input.GetTouch(0).position.x > 3 * Screen.width / 4 && Mathf.Abs(secondpoint.y - firstpoint.y) > 200)
                {
                    if (secondpoint.y < firstpoint.y)
                    {
                        StartCoroutine(rotateRight());
                    }
                    else
                    {
                        StartCoroutine(rotateLeft());
                    }

                }
                break;
        }
       
    }

    public IEnumerator rotateLeft()
    {
        Vector3 targetPoint = new Vector3(90, 0, 0);
        // transform.position += targetPoint;
        Vector3 rot = transform.rotation.eulerAngles;
        rot = new Vector3(rot.x, rot.y + 90, rot.z);
        Quaternion q = Quaternion.Euler(rot);
        float totalTime = 1.25f;
        float startTime = Time.time;
        float endTime = startTime + 0.5f;
        while (Time.time < endTime)
        {
            float timeSoFar = Time.time - startTime;
            float fractionTime = timeSoFar / totalTime;
            //StartCoroutine(wait());
            transform.rotation = Quaternion.Lerp(transform.rotation, q, fractionTime);
            yield return null;
        }
    }
    public IEnumerator rotateRight()
    {
        Vector3 targetPoint = new Vector3(90, 0, 0);
        // transform.position += targetPoint;
        Vector3 rot = transform.rotation.eulerAngles;
        rot = new Vector3(rot.x, rot.y - 90, rot.z);
        Quaternion q = Quaternion.Euler(rot);
        float totalTime = 1.25f;
        float startTime = Time.time;
        float endTime = startTime + 0.5f;
        while (Time.time < endTime)
        {
            float timeSoFar = Time.time - startTime;
            float fractionTime = timeSoFar / totalTime;
           // StartCoroutine(wait());
            transform.rotation = Quaternion.Lerp(transform.rotation, q, fractionTime);
            yield return null;
        }
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(1f);
    }
}
