﻿using UnityEngine;
using System.Collections;

public abstract class Figure {

    private int[] position;
    private ColorEnum color;
    private FigureTypeEnum type;

    public Figure(int[] pos, ColorEnum c, FigureTypeEnum t)
    {
        this.position = pos;
        this.color = c;
        this.type = t;
    }

    public int[] getPosition()
    {
        return this.position;
    }

    public void setPosition(int[] pos)
    {
        this.position = pos;
    }

    public ColorEnum getColor()
    {
        return this.color;
    }

    public void setColor(ColorEnum c)
    {
        this.color = c;
    }

    public FigureTypeEnum getType()
    {
        return this.type;
    }

    public void setType(FigureTypeEnum t)
    {
        this.type = t;
    }
}
