﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InitGameBoard : MonoBehaviour {

    private int width = 8;
    private int length = 8;

    List<Figure> figures; 
	// Use this for initialization
	void Start () {

        initializeGameBoard();
	}

    private void initializeGameBoard()
    {
        Debug.Log("ASD");
        for(int i = 0; i < width; i++)
        {
            for(int j = 0; j < length; j++)
            {
                GameObject cell = (GameObject)Instantiate(Resources.Load("Cell"));
                cell.transform.position = new Vector3(i, -0.1f, j) * 5;
                cell.transform.localScale += new Vector3(1, 0, 1) * 4;
                if((i+j) %2 == 0)
                {
                    cell.GetComponent<Renderer>().material.color = Color.black;
                }else
                {
                    cell.GetComponent<Renderer>().material.color = Color.white;
                }
                if (i == 1 || i == 6)
                {
                    if(i==1)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Pawn"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        go.GetComponent<Renderer>().material.color = Color.white;
                    }
                    if (i == 6)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Pawn"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        go.GetComponent<Renderer>().material.color = Color.black;
                    }
                }
                if( i == 0 )
                {
                    if (j == 0 || j == 7)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Rook"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        go.GetComponentInChildren<Renderer>().material.color = Color.white;
                    }
                    if (j==1 || j == 6)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Knight"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        go.GetComponent<Renderer>().material.color = Color.white;
                    }
                    if (j == 2 || j == 5)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Bishop"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        Renderer[] rends = go.GetComponentsInChildren<Renderer>();
                        foreach (Renderer r in rends)
                        {
                            r.material.color = Color.white;
                        }
                    }
                    if (j == 3)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Queen"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        Renderer[] rends = go.GetComponentsInChildren<Renderer>();
                        foreach (Renderer r in rends)
                        {
                            r.material.color = Color.white;
                        }
                    }
                    if (j == 4)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("King"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        Renderer[] rends = go.GetComponentsInChildren<Renderer>();
                        foreach (Renderer r in rends)
                        {
                            r.material.color = Color.white;
                        }
                    }
                }
                if (i == 7)
                {
                    if (j == 0 || j == 7)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Rook"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        go.GetComponentInChildren<Renderer>().material.color = Color.black;
                    }
                    if (j == 1 || j == 6)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Knight"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        go.GetComponent<Renderer>().material.color = Color.black;
                    }
                    if (j == 2 || j == 5)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Bishop"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        Renderer[] rends = go.GetComponentsInChildren<Renderer>();
                        foreach (Renderer r in rends)
                        {
                            r.material.color = Color.black;
                        }
                    }
                    if (j == 3)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("Queen"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        Renderer[] rends = go.GetComponentsInChildren<Renderer>();
                        foreach(Renderer r in rends)
                        {
                            r.material.color = Color.black;
                        }
                    }
                    if (j == 4)
                    {
                        GameObject go = (GameObject)Instantiate(Resources.Load("King"));
                        go.transform.position = new Vector3(i, 0, j) * 5;
                        Renderer[] rends = go.GetComponentsInChildren<Renderer>();
                        foreach (Renderer r in rends)
                        {
                            r.material.color = Color.black;
                        }
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
