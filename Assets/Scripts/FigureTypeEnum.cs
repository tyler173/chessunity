﻿using UnityEngine;
using System.Collections;

public enum FigureTypeEnum {

	KING, BISHOP, KNIGHT, PAWN, QUEEN, ROOK
}
